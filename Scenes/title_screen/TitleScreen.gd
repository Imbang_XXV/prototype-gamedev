extends Control

var scene_path_to_load

func _ready():
	$Container/NewGame.grab_focus()
	for button in $Container.get_children():
		button.connect("pressed", self, "on_Button_pressed", [button.scene_to_load])

func on_Button_pressed(scene_to_load):
	scene_path_to_load = scene_to_load
#	$FadeIn.show()
#	$FadeIn.fade_in()
#	print("dia")
	if scene_path_to_load == 'none':
		get_tree().quit()
	else:
		get_tree().change_scene(scene_path_to_load)

#func _on_FadeIn_fade_finished():
#	if scene_path_to_load == 'none':
#		get_tree().quit()
#	else:
#		get_tree().change_scene(scene_path_to_load)
