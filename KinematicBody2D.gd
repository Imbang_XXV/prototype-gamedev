extends KinematicBody2D

var ghost = self
onready var target = get_node("/root/ground_floor/Player")
var velocity = Vector2()
var speed = 4000

func _ready():
	set_physics_process(true)

func _process(delta):
	if target.position.x < position.x:
		velocity.x -= speed
	if target.position.y < position.y:
		velocity.y -= speed
	

func _physics_process(delta):
	# you get the position of the player
	var player_pos = target.get_global_transform().origin

	# you get the position of the enemy
	var ghost_pos = ghost.get_global_transform().origin
	
	var offset = player_pos - ghost_pos
	move_and_slide(offset.normalized() * speed * delta)

func find_path_to_player(ghost_pos, player_pos):
	return get_node("/root/ground_floor/Navigation2D").get_simple_path(ghost_pos,player_pos)

func _on_Ghost_area_entered(area):
	if area.is_in_group("player"):
		print("success")
		get_tree().change_scene("res://Scenes/title_screen/TitleScreen.tscn")
